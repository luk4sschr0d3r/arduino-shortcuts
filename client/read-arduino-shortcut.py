import serial
import subprocess
import sys

conn = serial.Serial('/dev/ttyACM0', 9600)

commands = [
'wmctrl -a "Force.com Developer Console - Mozilla Firefox"',
"/bin/bash -c 'echo 22'",
"/bin/bash -c 'echo 33'",
"/bin/bash -c 'echo 44'",
"/bin/bash -c 'echo 55'",
"/bin/bash -c 'echo 66'",
"/bin/bash -c 'echo 77'",
'wmctrl -a "Eclipse"',
]

def do_exec(num):
	cmd = commands[int(num) - 1]
	print >>sys.stderr, "Executing command: ", cmd
	status = subprocess.call(cmd, shell = True)
	print >>sys.stderr, "Execution status: ", status
	pass

try:
	while True:
		line = conn.readline()
		cmd = line.strip().split(":", 2)
		if cmd[0] == "exec":
			do_exec(cmd[1]);
except KeyboardInterrupt:
	print "Terminating"


