#include <EEPROM.h>
#include <SPI.h>
#include <GD2.h>

// foreground and background colors for boxes
int fColor[] = { 0xffffff, 0x000000 };
int bColor[] = { 0x000000, 0xffffff };

int val = 0;
byte seen[255];

int currently_pushed = 0;
int last_sent = 0;
unsigned long last_send_time = 0;

void setup()
{
  Serial.begin(9600);        // connect to the serial port
  GD.begin();
  
  for (int i = 0; i < 255; i++) {
    seen[i] = 0;
  }
}

void draw_box(int which)
{
  // parameters for the boxes
  int perRow = 4;
  int height = 50;
  int width = 80;
  int padding = 20;
  
  int corner1X, corner1Y;
  
  int oddOrEven = which % 1;
  
  // initial offset
  corner1X = 20;
  corner1Y = 80; 
  
  int row = which / perRow;
  int col = which % perRow;

  corner1X = corner1X + col * (width + padding) + col * padding;
  corner1Y = corner1Y + row * (height + padding) + row * padding;

  GD.Tag(which + 1);

  GD.ColorRGB(bColor[oddOrEven]);  
  GD.Begin(RECTS);
  GD.LineWidth(10 * 16);                    // corner radius 10.0 pixels

  GD.Vertex2ii(corner1X, corner1Y);
  GD.Vertex2ii(corner1X + width, corner1Y + height);
  
  String i2s = String(which + 1);
  char number[i2s.length()+1];
  
  i2s.toCharArray(number, i2s.length() + 1);
  
  GD.ColorRGB(fColor[oddOrEven]);
  GD.cmd_text(corner1X + (width / 2), corner1Y + (height / 2), 31, OPT_CENTER, number);
  
}


void loop()
{
  GD.get_inputs();

  if (currently_pushed == 0 && last_send_time + 1000 < millis())
  {
    // reset last sent command after 1s
    last_sent = 0;
  }
  
  GD.ClearColorRGB(0x058f5c);
  GD.Clear();

  for (int i = 0; i < 8; i++) {
    draw_box(i);
  }

  GD.swap();
  
  if (GD.inputs.x != -32768)
  {
    if (GD.inputs.tag > 0 && GD.inputs.tag < 255) {
      if (currently_pushed == 0) {
        currently_pushed = GD.inputs.tag;
      }
    }
  }
  else if (currently_pushed > 0)
  {
    if (last_sent != currently_pushed)
    {
      Serial.print("Pushed and released: tag: ");
      Serial.print(currently_pushed);
      Serial.println("");
    }
    
    last_send_time = millis();
    last_sent = currently_pushed;
    currently_pushed = 0;
  }
}


